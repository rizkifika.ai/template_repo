import os
import sys

def add_number(a,b):
    return int(a)+int(b)

def substract_number(a,b):
    return int(a)-int(b)

if __name__ == "__main__":
    print(add_number(10,5))
    print(substract_number(10,5))