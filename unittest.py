from math_func import add_number, substract_number

def test_add_number():
    val = add_number(10,5)
    assert val == 15

def test_substract_number():
    val = substract_number(10,5)
    assert val == 5
